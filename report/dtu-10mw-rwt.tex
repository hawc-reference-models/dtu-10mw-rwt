\documentclass[10pt,a4paper]{article}
\usepackage[utf8]{inputenc}
\usepackage{amsmath}
\usepackage{amsfonts}
\usepackage{amssymb}
\usepackage{graphicx}
\usepackage{pdflscape}
\usepackage{enumitem}
\usepackage{multirow}
\usepackage{hyperref}  % mailto hyperlink
\usepackage{array}  % raggedright in array cells

%\newenvironment{tablist}{\vspace*{-18pt} 
%	\begin{itemize}{}\itemsep-3pt\itemindent-12pt
%}{
%	\end{itemize}
%}
\newenvironment{tablist}{
	\begin{itemize}[nosep, leftmargin=9pt]
}{
	\end{itemize}
}


\title{The DTU 10 MW RWT HAWC Model}
\author{DTU Wind Energy}
\date{}

\begin{document}

\maketitle

% ----------------------------------------------------------------
\section{Introduction}

This repository (``repo'') contains a DLB-ready HAWC2 implementation of the DTU 10 MW Reference Wind Turbine (RWT).
This model is one of the most robust models curated by DTU Wind Energy, and it has been used quite extensively in a wide range of research activities.
A description of the original model design can be found in the design report\footnote{C. Bak \emph{et al}. ``Description of the DTU 10 MW Reference Wind Turbine.'' DTU Wind Energy Report-I-0092}.

% dlc table
\begin{table}\centering
	\caption{List of DLCs for which model has been tested.
		A blank ``Notes'' cell indicates that the model has not been tested and/or is not applicable to that DLC.}
	\begin{tabular}{p{3cm}|rl|p{4.5cm}}
		Category & \multicolumn{2}{l}{DLC} & Notes \\\hline
		\multirow{4}{3cm}{Normal power production} & 
			1.1/1.2 & NTM &  \\
		 & 1.3 & ETM &  \\
		 & 1.4 & ECD &  \\
		 & 1.5 & EWS &  \\\hline
		\multirow{5}{3cm}{Power production with fault} 
		 & 2.1 & NTM &  \\
		 & 2.2 & NTM &  \\
		 & 2.3 & EOG &  \\
		 & 2.4 & NTM &  \\
		 & 2.5 & NWP &  \\\hline
		\multirow{3}{3cm}{Start-up} 
		 & 3.1 & NWP &  \\
		 & 3.2 & EOG &  \\
		 & 3.3 & EDC &  \\\hline
		\multirow{2}{3cm}{Normal shutdown} 
		 & 4.1 & NWP &  \\
		 & 4.2 & EOG &  \\\hline
		\multirow{1}{3cm}{Emergency stop} 
		 & 5.1 & NTM &  \\\hline
		\multirow{4}{3cm}{Normal parked} 
		 & 6.1 & EWM &  \\
		 & 6.2 & EWM &  \\
		 & 6.3 & EWM &  \\
		 & 6.4 & NTM &  \\\hline
		\multirow{1}{3cm}{Parked with fault} 
		 & 7.1 & EWM &  \\\hline
		\multirow{2}{3cm}{Transport, assembly and maint/repair} 
		 & 8.1 & NTM &  \\
		 & 8.2 & EWM &  \\
	\end{tabular}
	\label{tab:dlcs}
\end{table}

% ----------------------------------------------------------------
\section{Model history and versioning}

The history of the DTU 10~MW is slightly disorganized, primarily due to two different sets of model files being developed in parallel: those hosted on the original RWT repo\footnote{\href{https://rwt.windenergy.dtu.dk}{https://rwt.windenergy.dtu.dk}} and a separate set of files that privately hosted and released on the HAWC2 website.
The two sets of input files were curated by different groups of researchers, and there was little synchronization between them.
The files hosted in this repository track the files that were distributed on the HAWC2 website.
However, because this repo is now a submodule of the RWT repo, the two modules should be synchronized in the future.

The model was first released as version 1.0, after which subsequent releases incremented by 1 until version 9.0.
The change in versioning pattern from 9.0 to 9.1 is likely because there is zero changes in the text files.
Thus, only the compiled DLLs were updated.
Early changes in the model (version 1.0 through 4.0) seem to have been primarily done by Lars Christian Andersen and Anders Yde of the Loads and Control (LAC) section, as indicated by comments in the htc file.
Other changes by Leondardo Bergami appear in version 4.0.
Further changes were of course made, but it is unclear where the contributions came from.
A log of the changes in the different versions is given in Tables~\ref{tab:version1} and \ref{tab:version2}.
Specific changes can be found by using the \texttt{diff} command in git (from a local clone of the repo) of by comparing branches on GitLab.

% ----------------------------------------------------------------
\section{Model structure}

The model is constructed according to the standard method for three-bladed horizontal-axis wind turbines in HAWC2.
There are 9 main bodies: tower, towertop, shaft, three hub elements and three blades.
Of these bodies, the towertop and hub elements are rigid.
The main bodies are summarized in Table~\ref{tab:bodies}.
The blades are subdivided into 10 bodies to better allow for large deflections.
The bodies are modeled as a mixture of flexible (tower, shaft [torsion only] and blades) and stiff (towertop and hubs) bodies.

As indicated in the reference documentation, the DTU 10 MW RWT has a gearbox ratio of 50.
However, the HAWC2 implementation does not use the gearbox ratio, choosing instead to simply model the generator inertia on the low-speed-shaft side.
It is for this reason that the gearbox ratio in the controller and generator servo DLLs is listed as 1 instead of 50.
Thus, all generator torque values and specifications are given on the low-speed shaft side.

% ----------------------------------------------------------------
\section{Repository structure}

This repo uses a branch structure to track the different model versions.
For example, version 1.0 of the model can be found in branch \texttt{v1.0}.
These branches are locked to prevent unauthorized pushes or merges.
Updates to comments and/or line order are allowed within a released version.
However, any changes to model parameters or outputs are not allowed.
If a user requires changes to a released version, they must do so on a separate branch, and these changes cannot be merged.
Updates to the model itself (e.g., fixing bugs or improving the controller) must be submitted as a merge request on the master branch and then reviewed/accepted by the repo responsible.

This repo contains the source code for this report, some utilities for model generation, and the HAWC2 and HAWCStab2 input files for the model.
The folder structure for the input files is as shown:
\begin{verbatim}
* control/  <-- control DLLs go here
   * wpdata.100  <-- text files for DTU Basic Controller
* data/  <-- files for bodies and HAWCStab2 operational data
* htc/  <-- HAWC2 files
   * DTU_10MW_RWT.htc  <-- turbulent simulation, 24 m/s
   * DTU_10MW_RWT_step.htc  <-- step-wind simulation
* DTU_10MW_RWT_hs2.htc  <-- HAWCStab2 file
\end{verbatim}
The turbulent simulation is a ``unique'' file from which the step-wind and HAWCStab2 files are automatically constructed using a Python script in the utilities folder.
The compiled control DLLs are not provided in the repo due to their binary nature.
If you a require a ready-to-run model with all DLLs, please download the zip file from the HAWC2 website.

% ----------------------------------------------------------------
\section{Bugs, model improvements and questions}

If you as a user notice that there is a bug in the model (e.g., a sign error or a parameter that doesn't match the model documentation), then we kindly ask that you notify us by submitting an issue on GitLab.
Navigate to the repo on GitLab, then click on ``Issues'' in the side bar.
The Issue Tracker on GitLab is how we keep track of issues with the model and things that should be fixed for the next model release.

If you have questions about a model that are not answered by this document, then please email \href{mailto:hawc2@windenergy.dtu.dk}{hawc2@windenergy.dtu.dk}.

% ----------------------------------------------------------------
% Table pages (landscape)

\begin{landscape}
% TABLE. Model versions, part 1.
\begin{table}\centering
	\caption{Summary of model versions (Part 1)}\vspace*{0.5cm}
	\begin{tabular}{c|p{4in}|p{4in}}
		Version & Notes/Changes to HAWC2 files &
			 Notes/Changes to HAWCStab2 files \\\hline
		9.1 & \begin{tablist}
			\item Updated DLLs.
			\end{tablist}
			&  \\\hline
		9.0 & \begin{tablist}
			\item Updated version number in header and simulation and output blocks.
			\item Updated comments in DTU Basic block and added new lines for new controller version.
			\item Updated array sizes in generator servo, mechanical brake and servo with limits.
			\item Replaced old tower clearance with \texttt{towerclearance\_mblade}.
			\end{tablist}
			& \begin{tablist}
			\item Added verbose output.
			\end{tablist} \\\hline
		8.0 & \begin{tablist}
			\item DLL source code removed.
			\item Version updated in htc header and log/simulation names.
			\item Total time to 1000 seconds and $\Delta t$ from 0.02 to 0.01.
			\item Controller changed to DTU Basic and retuned.
			\item Maximum brake torque updated and sign error in mechanical brake fixed again.
			\item Output channels updated.
			\end{tablist}
			& \begin{tablist}
			\item Operation data recalculated for different wind speeds (5 to 25), certain columns removed and renamed.
			\end{tablist}
	\end{tabular}
	\label{tab:version1}
\end{table}

% TABLE. Model versions, part 2.
\begin{table}\centering
	\caption{Summary of model versions (Part 2)}\vspace*{0.5cm}
	\begin{tabular}{c|p{4in}|p{4in}}
		Version & Notes/Changes to HAWC2 files &
			 Notes/Changes to HAWCStab2 files \\\hline
		7.0 &  & \begin{tablist}
			\item Added pitch controller block.
			\item Added quadratic example.
			\end{tablist}
			\\\hline
		6.0 & \begin{tablist}
			\item Typo removed in end of blade structural file.
			\end{tablist} & 
			\\\hline
		5.0 & \begin{tablist}
			\item DLL source code directory renamed.
			\item Sign error fixed in mechanical brake.
			\end{tablist} & 
			\\\hline
		4.0 & \begin{tablist}
			\item Small changes in comments of HAWC2 file.
			\item HAWC2 file changed from turbulent to step-wind.
			\item HAWC2 file updated and re-tuned for presumably a new version of the Risoe Controller.
			\item Mechanical brake added.
			\end{tablist} & 
			\begin{tablist}
			\item Small changes to comments.
			\item Updates to miscellaneous parameters in HAWCStab2 block (pitch actuator to 1 Hz, generator speed no longer integer, max power changed slightly, controller renamed to DTU basic)
			\item Changed from constant power to constant torque in HAWCStab2 block.
			\item Operation file regenerated and renamed.
			\end{tablist}
			\\\hline
		3.0 & \begin{tablist}
			\item Changes in DLL source code.
			\end{tablist} & 
			\begin{tablist}
			\item Updates to node numbers in HAWCStab2 block.
			\end{tablist}
			\\\hline
		2.0 & \begin{tablist}
			\item Addition of source code for HAWC2 DLLs.
			\item Mass-proportional damping removed on all bodies, damping on some bodies re-tuned.
			\end{tablist} & 
			\begin{tablist}
			\item Addition of HAWCStab2 htc and operational files.
			\end{tablist} 
			\\\hline
		1.0 & & \\
	\end{tabular}
	\label{tab:version2}
\end{table}

% TABLE. Bodies
\begin{table}\centering
	\caption{Main bodies in HAWC2 model.
	    ``X'' indicates index 1, 2 or 3.
	    Data files for all bodies contain two subsets: flexible and rigid.}\vspace*{0.5cm}
	\begin{tabular}{
	p{1.5cm}|
	>{\raggedright\arraybackslash}p{3cm}|
	>{\raggedright\arraybackslash}p{3cm}|
	p{1.5cm}|
	>{\raggedright\arraybackslash}p{3.5cm}|
	>{\raggedright\arraybackslash}p{5cm}}
        Body & Location & Constraint & Flexible? 
            & Coordinate system & Notes \\\hline
        Tower & Ground to bottom of nacelle & Bottom node fixed to ground & 
            Yes & z down, y downwind & \\\hline
        Towertop & Bottom of nacelle up to shaft & Fixed to tower & No &
            z down, y downwind & 
            Flexible parameters included in structural data file but by
            by default not enabled. \\\hline
        Shaft & Towertop to center of rotor, tilted. & Rotates at rotor speed 
            & Torsion only & z upwind along shaft & \\\hline
        HubX & Rotor center to blade root, coned & Fixed to shaft & No & 
            y downwind along shaft, z outwards towards blade & 
            Flexible parameters included in structural data file but by
            by default not enabled.\\\hline
        BladeX & Blade root to tip, coned & Fixed to hub & Yes &
            y downwind along shaft, z from root to tip & 
	\end{tabular}
	\label{tab:bodies}
\end{table}
\end{landscape}

% ----------------------------------------------------------------
%\section{HAWC2/HAWCStab2 Results}
% To be added later


\end{document}